

class PokerHand {
  constructor(props) {
    this.hand = props;
    this.firstPare = 1;
    this.secondPare = 1;
    this.suits = 1;
    this.flash = false;
  }

  getOutcome() {
    for (let i = 0; i < this.hand.length; i++) {
      let x = this.hand[i].rank;
      let secondNumber;
      this.suits = 1;
      for (let j = i + 1; j < this.hand.length; j++) {
        if (this.hand[i].suit === this.hand[j].suit) {
          this.suits++;
          if (this.suits === this.hand.length) {
            this.flash = true;
            break;
          }
        }
        if (this.hand[j].rank === x && this.firstPare !== 2 && this.firstPare !== 3) {
          this.firstPare++;
          secondNumber = this.hand[j].rank;
        } else if (x === secondNumber && x === this.hand[j].rank) {
          this.firstPare++;
        } else if (this.hand[j].rank === x && this.firstPare === 2) {
          this.secondPare++;
        }
      }
    }

  }
}

export default PokerHand;