
class CardDeck {

  constructor() {
    this.cardSuits = ['D', 'H', 'C', 'S'];
    this.cardRanks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
    this.cardDeck = [];
    this.fiveCards = [];

    for (let key in this.cardSuits) {
      for (let i in this.cardRanks) {
        let card = {
          suit: this.cardSuits[key],
          rank: this.cardRanks[i]
        };
        this.cardDeck.push(card);
      }
    }
  }

  getCard() {
    const random = Math.floor(Math.random() * this.cardDeck.length);
    this.currentRandom = random;
    return this.cardDeck[random];
  }

  getCards(howMany) {
    for (let i = 0; i < howMany; i++) {
      this.fiveCards.push(this.getCard());
      this.cardDeck.splice(this.currentRandom, 1);
    }
  }

}

export default CardDeck;