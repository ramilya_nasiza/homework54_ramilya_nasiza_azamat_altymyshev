import React, { Component } from 'react';
import Card from './components/Card/Card';
import CardDeck from './CardDeck';
import PokerHand from './PokerHand';

import './App.css';

class App extends Component {

  state = {
    cards: [
      {suit: '', rank: ''},
      {suit: '', rank: ''},
      {suit: '', rank: ''},
      {suit: '', rank: ''},
      {suit: '', rank: ''}
    ],
    text: ''
  };

  getNewCardDeck () {
    const newCardDeck = new CardDeck();
    newCardDeck.getCards(5);
    const cards = newCardDeck.fiveCards;
    this.setState({cards});

    const Pok = new PokerHand(cards);
    Pok.getOutcome();

    if (Pok.flash === true) {
      this.setState({text: 'У вас флеш! Красавчик!'});
    } else if (Pok.firstPare === 2 && Pok.secondPare === 2) {
      this.setState({text: 'Две пары!'});
    } else if (Pok.firstPare === 2 && Pok.secondPare === 1) {
      this.setState({text: 'Одна пара!'});
    } else if (Pok.firstPare === 3) {
      this.setState({text: 'У вас тройка! (сет)'});
    } else {
      this.setState({text: 'У вас ничего!'});
    }
  }

  render() {
    return (
      <div className="App">
        <button onClick={() => this.getNewCardDeck()}>Give cards</button>
        {this.state.cards.map((number, index) => {
          return <Card rank={number.rank} suit={number.suit} key={index}/>
        })}
        <p>{this.state.text}</p>
      </div>
    );
  }
}

export default App;
