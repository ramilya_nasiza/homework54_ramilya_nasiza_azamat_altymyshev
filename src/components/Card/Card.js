import React from 'react';

import './Card.css';

const Card = props => {

  const chooseSuit = (suit) => {
    switch (suit) {
      case 'D':
        return 'diams';
      case 'H':
        return 'hearts';
      case 'C':
        return 'clubs';
      case 'S':
        return 'spades';
      default:
    }
  };

  const chooseSuitsImg = (suit) => {
    switch (suit) {
      case 'D':
        return '♦';
      case 'H':
        return '♥';
      case 'C':
        return '♣';
      case 'S':
        return '♠';
      default:
    }
  };

  return (
      <div className={(props.rank !== '' && props.suit !== '') ? ("Card Card-rank-" + props.rank.toLowerCase() + " Card-" + chooseSuit(props.suit)): "Card Card-back"}>
        <span className="rank">{props.rank}</span>
        <span className="suit">{chooseSuitsImg(props.suit)}</span>
      </div>
  )
};

export default Card;